import swal from 'sweetalert';


export const  confirmbox =  async(title,message,callback) => {
    return await swal({
        title: title,
        text: message,
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        timer: 20000,
        buttons: {            
            no: {
                text: "Cancel",
                className: 'btn btn-secondary bg-secondary',
                value: false,
                closeModal: true
            },
            yes: {
                text: "Confirm",
                className: "btn btn-warning bg-warning",
                value: true,
                closeModal: true
            }
        }
      }).then((confirmed) => {
        if (confirmed) {
            callback(true);
        }else{
            callback(false);
        }
        return false;
      });
}