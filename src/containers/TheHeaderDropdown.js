import React from 'react'
import {
  //CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
  CButton
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { getUser,removeUserSession } from '../Utils/Common'
import {toast} from 'react-toastify'; 
import 'react-toastify/dist/ReactToastify.css'; 
const TheHeaderDropdown = (props) => {
  toast.configure() 
  const user = getUser();
  if(user==null){
    props.history.push('/login');
  }
  const handleLogout = () => {
    removeUserSession();
    toast.success('Logout Successfull');
    props.history.push('/login');
  }
  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={'avatars/7.jpg'}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">

        <CDropdownItem>
          <CButton color="link" onClick={handleLogout}  className="px-0"><CIcon name="cil-lock-locked" className="mfe-2" />
          Log Out</CButton>
          
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdown
