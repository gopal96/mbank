import React from 'react'
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader
} from './index'

const TheLayout = (props) => {

  return (
    <div className="c-app c-default-layout">
      <TheSidebar history={props.history} />
      <div className="c-wrapper">
        <TheHeader history={props.history} />
        <div className="c-body">
        
          <TheContent history={props.history}/>
        </div>
        <TheFooter/>
      
      </div>
    </div>
  )
}

export default TheLayout
