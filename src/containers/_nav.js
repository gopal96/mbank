
const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Stock',
    to: '/dashboard',
    badge: {
      color: 'info',
      text: 'NEW',
    }
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Portfolio',
    to: '/portfolio',
  },

]

export default _nav
