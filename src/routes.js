import React from 'react';
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const PortFolio = React.lazy(() => import('./views/portfolio/portFolio'));
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Stock', component: Dashboard },
  { path: '/portfolio', name: 'PortFolio', component: PortFolio },

];

export default routes;
