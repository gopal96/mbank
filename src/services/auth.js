import userData from '../services/login'


//function for login
async function login(data) {
    // toLowerCase
    let email=data.USER_EMAIL.toLowerCase();
    let password=data.USER_PASSWORD;
    if(email===userData.EMAIL && password===userData.PASSWORD){
        let res={msg:"login successfully",code:200,data:userData};
        return res;
    }else{
        let res={msg:"invalid credentials",code:400,data:""};
        return res;
    }

}


const authService = {
    login
};

export default authService;
