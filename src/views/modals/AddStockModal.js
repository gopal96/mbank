import React, { useState, useEffect } from 'react'
import {
  CButton,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react'
import wishList from '../../services/wishList'
import { validate } from "../validate";
import { confirmbox } from '../../Utils/Alertprompt';
import { setUserSessionPortfolio, getPortfolioData } from '../../Utils/Common';
const Keys = {
  "SHARE_SYMBOL": "SHARE_SYMBOL",
  "SHARE_QTY": "SHARE_QTY",
  "SHARE_TOTAL": "SHARE_TOTAL",
}
const Formdata = {
  SHARE_ID: null,
  SHARE_SYMBOL: "",
  SHARE_QTY: null,
  SHARE_TOTAL: null,
  SHARE_PRICE: null,

}
const validationRules = [
  {
    field: "SHARE_QTY",
    validations: ["required"],
    name: "Share Quantity",
  }
];
const AddStockModal = (isOpen, toggleModal) => {
  const [scriptData, setScriptData] = useState(false);
  const [formData, setFormData] = useState(Formdata);
  const [errorsData, setErrors] = useState(Formdata);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getSelectedData();
  }, [isOpen.shareid])
  const getSelectedData = () => {
    if (isOpen.shareid) {
      setFormData({
        ...formData,
        ["SHARE_QTY"]: parseInt(0),
      });
      var filterResult = wishList.filter(
        (wishdata) => wishdata.ID === isOpen.shareid
      )[0];
      let mykeys = {
        SHARE_ID: filterResult.ID,
        SHARE_SYMBOL: filterResult.SCRIP_NAME,
        SHARE_TOTAL: 0,
        SHARE_QTY: null,
        SHARE_PRICE: filterResult.LTP,
      }
      setFormData(mykeys);
      setScriptData(filterResult);
    }
  };
  //this function for change quantity calculation
  const handleChangeQty = (event) => {
    const { target } = event;
    const val = target.value;
    const { name } = target;
    var total = parseInt(val) * scriptData.LTP;
    setFormData({
      ...formData,
      ["SHARE_QTY"]: parseInt(val),
      ["SHARE_TOTAL"]: parseInt(total)
    });
  };
  // reset the form after submit buy share form
  function resetitem() {
    let mykeys = {
      SHARE_ID: null,
      SHARE_SYMBOL: "",
      SHARE_QTY: "",
      SHARE_TOTAL: "",
      SHARE_PRICE: "",
    }
    setFormData(mykeys);
  }
  //validation
  function validates() {
    const fieldss = { fields: formData };
    const { isValid, errors } = validate(fieldss, validationRules);
    setErrors(errors);
    return isValid;
  }
  // this function used for buying share
  function handlesubmit(event) {
    event.preventDefault()
    if (validates()) {
      setLoading(true);
      confirmbox('Confirm action!', 'Are you sure want to buy this share?', function (confirmed) {
        if (confirmed) {
          let portfolioData = getPortfolioData();
          var parray = [];
          if (portfolioData) {
            for (let i = 0; i < portfolioData.length; i++) {
              parray.push(portfolioData[i])
            }
          } {
            parray.push(formData)
          }
          setUserSessionPortfolio(parray);
          resetitem();
          isOpen.toggleModal();
          setLoading(false);
        } else {
          setLoading(false);
        }
      });

    }
  }
  // render
  return (
    <CModal
      show={isOpen.isOpen}
      onClose={isOpen.toggleModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>{scriptData.SCRIP_FULL_NAME} </CModalTitle>
        <br></br>

      </CModalHeader>
      <CModalBody>
        <CForm onSubmit={handlesubmit}>
          <CFormGroup row className="my-0">
            <span className='text-right'>price {scriptData.LTP}</span>
            <CCol xs="4">
              <CFormGroup>
                <CLabel htmlFor="vat">Quantity</CLabel>
                <CInput
                  type="number"
                  name={Keys.SHARE_QTY}
                  id={Keys.SHARE_QTY}
                  value={formData[Keys.SHARE_QTY] || ""}
                  onChange={(event) => {
                    handleChangeQty(event);
                  }}
                  placeholder="quantity"

                />
                <CLabel
                  style={{
                    display: errorsData.SHARE_QTY
                      ? "block"
                      : "none",
                  }}
                  className="errors"
                >
                  {errorsData.SHARE_QTY}
                </CLabel>
              </CFormGroup>
            </CCol>
            <CCol xs="4">
              <CFormGroup>
                <CLabel>Total Investment</CLabel>
                {/* SHARE_TOTAL */}
                <CInput
                  type="text"
                  name={Keys.SHARE_TOTAL}
                  id={Keys.SHARE_TOTAL}
                  value={formData[Keys.SHARE_TOTAL] || 0}

                  placeholder="Total"
                  readOnly={true}
                />

              </CFormGroup>
            </CCol>
            <CCol xs="4" className="">
              <CButton type="submit" disabled={loading} color="info" className="search-cu-button">{loading ? 'Please Wait...' : 'Buy'}</CButton>&nbsp;&nbsp;

            </CCol>
          </CFormGroup>
        </CForm>
      </CModalBody>
      <CModalFooter>

        <CButton
          color="secondary"
          onClick={isOpen.toggleModal}
        >Cancel</CButton>
      </CModalFooter>
    </CModal>
  )
}

export default AddStockModal
