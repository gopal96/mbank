import React, { useState, useEffect, useRef } from 'react'
import {
  CButton,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react'
import wishList from '../../services/wishList'
import { toast } from 'react-toastify';
import priceData from '../../services/priceData';
import volumeData from '../../services/volumeData';
import { createChart, CrosshairMode } from 'lightweight-charts';
const SearchValue = {
  FROM: "",
  TO: "",
}
const searchKeys = {
  "FROM": "FROM",
  "TO": "TO",
}
const MyPortfolioModal = (isOpen) => {
  const [searchValue, setSearchValue] = useState(SearchValue);
  const [searchData, setSearchData] = useState(priceData);
  const [searchVolumeData, setSearchVolumeData] = useState(volumeData);
  const [scriptData, setScriptData] = useState(false);
  useEffect(() => {
    getSelectedData();
  }, [isOpen.shareid])
  useEffect(() => {
  }, [searchData]);

  const getSelectedData = () => {
    if (isOpen.shareid) {
      var filterResult = wishList.filter(
        (wishdata) => wishdata.ID === isOpen.shareid
      )[0];
      setScriptData(filterResult);
    }
  };
  const chartContainerRef = useRef();
  const chart = useRef();
  const resizeObserver = useRef();
  useEffect(() => {
    chart.current = createChart(chartContainerRef.current, {
      width: chartContainerRef.current.clientWidth,
      height: chartContainerRef.current.clientHeight,
      layout: {
        backgroundColor: '#253248',
        textColor: 'rgba(255, 255, 255, 0.9)',
      },
      grid: {
        vertLines: {
          color: '#334158',
        },
        horzLines: {
          color: '#334158',
        },
      },
      crosshair: {
        mode: CrosshairMode.Normal,
      },
      priceScale: {
        borderColor: '#485c7b',
      },
      timeScale: {
        borderColor: '#485c7b',
      },
    });
    const candleSeries = chart.current.addCandlestickSeries({
      upColor: '#4bffb5',
      downColor: '#ff4976',
      borderDownColor: '#ff4976',
      borderUpColor: '#4bffb5',
      wickDownColor: '#838ca1',
      wickUpColor: '#838ca1',
    });
    candleSeries.setData(searchData);
    const volumeSeries = chart.current.addHistogramSeries({
      color: '#182233',
      lineWidth: 2,
      priceFormat: {
        type: 'volume',
      },
      overlay: true,
      scaleMargins: {
        top: 0.8,
        bottom: 0,
      },
    });

    volumeSeries.setData(searchVolumeData);
  }, [searchData]);
  // Resize chart on container resizes.
  useEffect(() => {
    resizeObserver.current = new ResizeObserver(entries => {
      const { width, height } = entries[0].contentRect;
      chart.current.applyOptions({ width, height });
      setTimeout(() => {
        chart.current.timeScale().fitContent();
      }, 0);
    });
    resizeObserver.current.observe(chartContainerRef.current);
    return () => resizeObserver.current.disconnect();
  }, [searchData]);
  const handleChangeDate = (event) => {

    const { target } = event;
    const val = target.value;
    const { name } = target;
    setSearchValue({
      ...searchValue, [name]: val
    });
  };
  // this function used for search value
  function handlesearch(event) {
    event.preventDefault()
    var searchDataValue = [];
    var searchValumeDataValue = [];
    if (searchValue.FROM > searchValue.TO) {
      toast.error("From date should be greater than To date");
    } else {
      for (let j = 0; j < priceData.length; j++) {
        let days = priceData[j].time.day < 10 ? '0' + priceData[j].time.day : priceData[j].time.day;
        let months = priceData[j].time.month < 10 ? '0' + priceData[j].time.month : priceData[j].time.month;
        if (priceData[j].time) {
          let dates = priceData[j].time.year + '-' + months + '-' + days;
          if (dates >= searchValue.FROM && dates <= searchValue.TO) {
            searchDataValue.push(priceData[j]);
          }
        }
      }
      for (let j = 0; j < volumeData.length; j++) {
        let days = volumeData[j].time.day < 10 ? '0' + volumeData[j].time.day : volumeData[j].time.day;
        let months = volumeData[j].time.month < 10 ? '0' + volumeData[j].time.month : volumeData[j].time.month;
        if (volumeData[j].time) {
          let dates = volumeData[j].time.year + '-' + months + '-' + days;
          if (dates >= searchValue.FROM && dates <= searchValue.TO) {
            searchValumeDataValue.push(volumeData[j]);
          }
        }
      }
      const elements = document.getElementsByClassName("tv-lightweight-charts");
      while (elements.length > 0) elements[0].remove();
      setSearchData(searchDataValue);
      setSearchVolumeData(searchDataValue);
    }
  }
  // render
  return (
    <CModal
      show={isOpen.isOpen}
      onClose={isOpen.toggleModal}
      size={'xl'}
    >
      <CModalHeader closeButton>
        <CModalTitle>{scriptData.SCRIP_FULL_NAME} </CModalTitle>
      </CModalHeader>
      <CModalBody >
        <div >
          <CForm onSubmit={handlesearch} className='d-flex '>
            <CFormGroup className={'mr-3'}>
              <CLabel>From</CLabel>
              <CInput
                type="date"
                name={searchKeys.FROM}
                id={searchKeys.FROM}
                value={searchValue[searchKeys.FROM]}
                onChange={(event) => {
                  handleChangeDate(event);
                }}
                required={true}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>To</CLabel>
              <CInput
                type="date"
                name={searchKeys.TO}
                id={searchKeys.TO}
                value={searchValue[searchKeys.TO]}
                onChange={(event) => {
                  handleChangeDate(event);
                }}
                required={true}
              />
            </CFormGroup>
            <CFormGroup>
              <CButton type="submit" color="info" className="ml-3 search-cu-button">{'Search'}</CButton>
            </CFormGroup>
          </CForm>
        </div>
        <div id='chartnew'>
          <div ref={chartContainerRef} className="chart-container" />
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          color="secondary"
          onClick={isOpen.toggleModal}
        >Cancel</CButton>
      </CModalFooter>
    </CModal>
  )
}

export default MyPortfolioModal
