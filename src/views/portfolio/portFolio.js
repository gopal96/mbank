import React, { useEffect, useState } from 'react'
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow

} from '@coreui/react'
import 'react-toastify/dist/ReactToastify.css';
import {  getPortfolioData } from '../../Utils/Common';
import wishList from '../../services/wishList'
const fields = ['Sr No', 'SHARE SYMBOL', 'SHARE QTY', 'SHARE TOTAL']
const PortFoliolist = () => {
    const [myportFolioList, setmyportFolioList] = useState([]);
    const [totalInvValue, settotalInvValue] = useState([]);

    useEffect(() => {
        getmyPortFolio();
    }, []);

    //Here we are calculating total investing value for portfolio
    useEffect(() => {
        let total_investment = 0;
        for (let j = 0; j < myportFolioList.length; j++) {
            total_investment += myportFolioList[j].SHARE_TOTAL;

        }
        settotalInvValue(total_investment)
    }, [totalInvValue]);

    //for geting total buying in shares
    const getmyPortFolio = (event) => {
        let portfolioData = getPortfolioData();
        if (portfolioData) {
            var final_array = [];
            for (let i = 0; i < wishList.length; i++) {
                var check = 0;
                var parray = [];
                for (let j = 0; j < portfolioData.length; j++) {
                    if (wishList[i].ID === portfolioData[j].SHARE_ID) {
                        parray.push(portfolioData[j]);
                        check = 1;
                    }
                }
                var quantity = 0;
                var totalamount = 0;
                for (let k = 0; k < parray.length; k++) {
                    quantity += parray[k].SHARE_QTY;
                    totalamount += parray[k].SHARE_TOTAL;
                }
                if (check === 1) {
                    final_array.push({ id: wishList[i].ID, SHARE_QTY: quantity, SHARE_TOTAL: totalamount, SHARE_SYMBOL: wishList[i].SCRIP_FULL_NAME })
                }

            }
            setmyportFolioList(final_array)
        }
    }

    return (
        <>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <span className='text-right'> <b>Total Investment Amount: </b>{totalInvValue}</span>
                        </CCardHeader>

                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            PortFolio List
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={myportFolioList}
                                fields={fields}
                                hover={true}
                                striped={true}
                                bordered={true}
                                outlined={true}
                                responsive={true}
                                size="sm"
                                itemsPerPage={10}
                                scopedSlots={{
                                    'Sr No':
                                        (item, index) => (
                                            <td>

                                                {index + 1}
                                            </td>
                                        ),
                                    'SHARE SYMBOL':
                                        (item) => (
                                            <td>
                                                {item.SHARE_SYMBOL}
                                            </td>
                                        ),
                                    'SHARE QTY':
                                        (item) => (
                                            <td>
                                                {item.SHARE_QTY}
                                            </td>

                                        ),
                                    'SHARE TOTAL':
                                        (item) => (
                                            <td>
                                                {item.SHARE_TOTAL}
                                            </td>
                                        )
                                }}
                            />

                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default PortFoliolist
